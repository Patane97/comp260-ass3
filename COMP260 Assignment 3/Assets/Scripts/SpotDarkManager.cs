﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
[RequireComponent(typeof(AudioSource))]
public class SpotDarkManager : MonoBehaviour
{
    //Spotlight and Darkness Manager
    private bool darkness = false;
    public static bool spotlight = false;
    public GameObject SpotLightPrefab;
    public GameObject SpotLightFlickerPrefab;
    public SpotlightDetect SpotLightDetector;
    public static float playerhealth = 100f;
    private bool gameover = false;
    //Time Manager
    public static float SettimeInterval = 5f;
    private float timeInterval = SettimeInterval;
    public static float SetdarkInterval = 5f;
    private float darkInterval = SetdarkInterval;
    public static float round = 1;
    //Camera Background Colour Manager
    new Camera camera;
    private Color white = Color.white;
    private Color black = Color.black;
    private Color yellow = Color.yellow;
    //Spotlight spawner manager
    public float xMin, yMin;
    public float width, height;
    public float spotx, spoty;
    private bool flick = false;
    private new bool light = false;
    private float flickerInterval;
    //Canvas Managers
    public int playhealth = (int)playerhealth;
    private string healthstring;
    public Text healthText;
    public Text timerText;
    public Text roundText;
    public Text GameOverText;
    //Audio Manager
    public AudioClip playerhurt;
    public AudioClip GameOver;
    public AudioClip nextRound;
    private AudioSource audio;
    //Sprite Manager
    public Sprite partialdamage;
    public Sprite moderatedamage;
    public Sprite criticaldamage;
    public Sprite maxdamage;
    private SpriteRenderer playerSprite;





    //____________________________________________________________________________
    void Start()
    {
        GameOverText.enabled = false;
        camera = GetComponent<Camera>();
        audio = GetComponent<AudioSource>();
        PlayerMovement player = (PlayerMovement)FindObjectOfType(typeof(PlayerMovement));
        playerSprite = player.GetComponent<SpriteRenderer>();
    }


    void Awake()
    {
        //Generate random coordinates to spawn in spotlight
        spotx = xMin + Random.value * width;
        spoty = yMin + Random.value * height;
    }
    // Update is called once per frame
    //_______________________________________________________________
    void Update()
    {
        //Counts down the time till darkness approaches
        //Also spawns in the spotlight flickering
        if (gameover == false)
        {
            if (darkness == false)
            {
                //While counting down
                if (timeInterval > 0)
                {
                    timeInterval -= Time.deltaTime;
                    timerText.color = black;
                    timerText.text = timeInterval.ToString("f1");
                    healthstring = playhealth.ToString();
                    healthText.color = black;
                    healthText.text = "Health: " + healthstring;
                    roundText.text = "Round: " + round.ToString();
                    roundText.color = black;
                    flickerInterval = Mathf.Round(timeInterval * 10);
                    //Differing flicker rates depending on time left                            
                    if (timeInterval > 3.0f)
                    {
                        if (flick == false && flickerInterval % 4 == 0)
                        {
                            SpotLightFlickerSpawner();
                        }
                        if (flick == true && flickerInterval % 8 == 0)
                        {
                            SpotLightFlickerDestroyer();
                        }
                    }
                    else if (timeInterval > 1.0f && timeInterval <= 3.0f)
                    {
                        if (flick == false && flickerInterval % 2 == 0)
                        {
                            SpotLightFlickerSpawner();
                        }
                        if (flick == true && flickerInterval % 4 == 0)
                        {
                            SpotLightFlickerDestroyer();
                        }
                    }
                    else if (timeInterval <= 1.0f)
                    {
                        if (flick == false && flickerInterval % 1 == 0)
                        {
                            SpotLightFlickerSpawner();
                        }
                        if (flick == true && flickerInterval % 2 == 0)
                        {
                            SpotLightFlickerDestroyer();
                        }
                    }

                    //Finished countdown
                }
                else if (timeInterval < 0)
                {
                    darkness = true;
                    if (SettimeInterval >= 2.6f)
                    {
                        SettimeInterval -= 0.2f;
                    }
                    else if (SettimeInterval < 2.6f)
                    {
                        SettimeInterval = 2.6f;
                    }
                    timeInterval = SettimeInterval;

                }
            }

            /*When darkness becomes true, change background colour to black
            Count down a new timer, spawn the spotlight
            check if player is within spotlight
             */
            if (darkness == true)
            {
                healthText.color = yellow;
                roundText.text = "Round: " + round.ToString();
                roundText.color = yellow;
                //Create Spotlight
                if (light == false)
                {
                    SpotLightSpawner();
                }
                camera.backgroundColor = black;

                //Countdown
                if (darkInterval > 0)
                {
                    SpotLightDetector = FindObjectOfType<SpotlightDetect>();
                    SpotLightDetector.detect(spotx, spoty);
                    //If player isn't in spotlight, rapidly lose health
                    if (spotlight == false)
                    {
                        //Subtract health and update text values
                        playerhealth -= 50 * Time.deltaTime;
                        playhealth = (int)playerhealth;
                        healthstring = playhealth.ToString();
                        healthText.text = "Health: " + healthstring;
                        audio.PlayOneShot(playerhurt);
                        if(playerhealth <= 75)
                        {
                            playerSprite.sprite = partialdamage; 
                        }
                        if(playerhealth <= 50)
                        {
                            playerSprite.sprite = moderatedamage;
                        }
                        if(playerhealth <= 25)
                        {
                            playerSprite.sprite = criticaldamage;
                        }
                        if (playerhealth <= 0)
                        {
                            gameover = true;
                            GameOverText.enabled = true;
                            Time.timeScale = 0; //Gameover
                            GameOverText.text = GameOverText.text + " You made it to round number: " + round;
                            audio.PlayOneShot(GameOver);
                            playerSprite.sprite = maxdamage;
                        }
                    }
                    timerText.color = yellow;
                    timerText.text = darkInterval.ToString("f1");
                    darkInterval -= Time.deltaTime;
                }
                else if (darkInterval < 0)
                {
                    darkness = false;
                    SpotLightDestroyer();
                    camera.backgroundColor = white;
                    spotx = xMin + Random.value * width;
                    spoty = yMin + Random.value * height;

                    SetdarkInterval = SettimeInterval;

                    darkInterval = SetdarkInterval;
                    round++;
                    audio.PlayOneShot(nextRound);
                }

            }
        }
    }


//SPAWNERS AND DESTROYERS_________________________________________________________________________
//SpotLight
public void SpotLightSpawner()
    {
        for (int i = 0; i < 1; i++)
        { 
            GameObject SpotLight = Instantiate(SpotLightPrefab) as GameObject;
            SpotLight.transform.parent = transform;     
            SpotLight.transform.position = new Vector2(spotx, spoty);
            if (round < 17)
            {
                SpotLight.transform.localScale = new Vector2((1 - (round / 20)), (1 - (round / 20)));
                CircleCollider2D collider = SpotLight.GetComponent<CircleCollider2D>();
                collider.radius = (0.9f - (round / 20));
            } else if (round >= 17)
            {
                SpotLight.transform.localScale = new Vector2(0.15f,0.15f);
                CircleCollider2D collider = SpotLight.GetComponent<CircleCollider2D>();
                collider.radius = (0.05f);
            }
        }
        light = true;
    }


    public void SpotLightDestroyer()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Destroy(child.gameObject);
        }
        light = false;
    }

//SpotLight Flicker
    public void SpotLightFlickerSpawner()
    {
        for (int i = 0; i < 1; i++)
        {
            GameObject SpotLightFlicker = Instantiate(SpotLightFlickerPrefab) as GameObject;
            SpotLightFlicker.transform.parent = transform;
            SpotLightFlicker.transform.position = new Vector2(spotx, spoty);
            if (round < 17)
            {
                SpotLightFlicker.transform.localScale = new Vector2((1 - (round / 20)), (1 - (round / 20)));
            } else if (round >= 17)
            {
                SpotLightFlicker.transform.localScale = new Vector2(0.15f,0.15f);
            }
        }
            flick = true;   
    }
    public void SpotLightFlickerDestroyer()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Destroy(child.gameObject);
        }
            flick = false;
        
    }
//______________________________________________________________________________________________________
}

        