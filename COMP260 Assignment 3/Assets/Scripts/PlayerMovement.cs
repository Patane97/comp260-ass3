﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]

public class PlayerMovement : MonoBehaviour {
    public float speed = 5f;
    private new Rigidbody2D rigidbody2D;
    public float screenx;
    public float screeny;


    // Use this for initialization
    void Start () {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    void FixedUpdate ()
    {
        if (((transform.position.x <= screenx) || (transform.position.x >= (-screenx))) && ((transform.position.y <= screeny) || (transform.position.y >= (-screeny))))
        {
            Vector2 direction;
            direction.x = Input.GetAxis("Horizontal");
            direction.y = Input.GetAxis("Vertical");
            rigidbody2D.velocity = direction * speed;
        }
        
    }

}
