﻿using UnityEngine;
using System.Collections;

public class SpotDarkManagerBackup : MonoBehaviour
{
    //Spotlight and Darkness Manager
    private bool darkness = false;
    public static bool spotlight = false;
    public GameObject SpotLightPrefab;
    public GameObject SpotLightFlickerPrefab;
    public SpotlightDetect SpotLightDetector;
    public float playerhealth = 100f;
    //Time Manager
    public static float SettimeInterval = 5f;
    public float timeInterval = SettimeInterval;
    public static float SetdarkInterval = 5f;
    public float darkInterval = SetdarkInterval;
    public static int wave = 0;
    //Camera Background Colour Manager
    new Camera camera;
    private Color white = Color.white;
    private Color black = Color.black;
    //Spotlight spawner manager
    public float xMin, yMin;
    public float width, height;
    public float spotx, spoty;
    public bool flick = false;
    public new bool light = false;
    public int flickerInterval;





    //____________________________________________________________________________
    void Start()
    {

        camera = GetComponent<Camera>();
    }


    void Awake()
    {
        //Generate random coordinates to spawn in spotlight
        spotx = xMin + Random.value * width;
        spoty = yMin + Random.value * height;
    }
    // Update is called once per frame
    //_______________________________________________________________
    void Update()
    {
        //Counts down the time till darkness approaches
        //Also spawns in the spotlight flickering
        if (darkness == false)
        {
            //While counting down
            if (timeInterval > 0)
            {
                timeInterval -= Time.deltaTime;
                flickerInterval = (int)timeInterval;               
                if (flickerInterval % 2 == 1 && flick == false)
                {
                    SpotLightFlickerSpawner();
                }
                if((flickerInterval % 2 == 0) && (flick = true))
                {
                    SpotLightFlickerDestroyer();
                }

                //Finished countdown
            }
            else if (timeInterval < 0)
            {
                darkness = true;
                timeInterval = SetdarkInterval;

            }
        }

        /*When darkness becomes true, change background colour to black
        Count down a new timer, spawn the spotlight
        check if player is within spotlight, if not, end the game and print score.
         */
        if (darkness == true)
        {
            //Create Spotlight
            if (light == false)
            {
                SpotLightSpawner();
            }
            camera.backgroundColor = black;

            //Countdown
            if (darkInterval > 0)
            {
                    SpotLightDetector = FindObjectOfType<SpotlightDetect>();
                    SpotLightDetector.detect(spotx, spoty);

                //If player isn't in spotlight, rapidly lose health
                if (spotlight == false)
                {
                    playerhealth -= 33 * Time.deltaTime;
                    Debug.Log(playerhealth);
                    if(playerhealth <= 0)
                    {
                        Time.timeScale = 0;
                    }
                }

                
                darkInterval -= Time.deltaTime;
            } else if (darkInterval < 0)
            {
                darkness = false;
                SpotLightDestroyer();
                camera.backgroundColor = white;
                spotx = xMin + Random.value * width;
                spoty = yMin + Random.value * height;
                darkInterval = SetdarkInterval;
                wave++;
            }
            
        }
    }



//SPAWNERS AND DESTROYERS_________________________________________________________________________
//SpotLight
public void SpotLightSpawner()
    {
        for (int i = 0; i < 1; i++)
        { 
            GameObject SpotLight = Instantiate(SpotLightPrefab) as GameObject;
            SpotLight.transform.parent = transform;     
            SpotLight.transform.position = new Vector2(spotx, spoty);
        }
        light = true;
    }


    public void SpotLightDestroyer()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Destroy(child.gameObject);
        }
        light = false;
    }

//SpotLight Flicker
    public void SpotLightFlickerSpawner()
    {
        for (int i = 0; i < 1; i++)
        {
            GameObject SpotLightFlicker = Instantiate(SpotLightFlickerPrefab) as GameObject;
            SpotLightFlicker.transform.parent = transform;
            SpotLightFlicker.transform.position = new Vector2(spotx, spoty);
        }
            flick = true;   
    }
    public void SpotLightFlickerDestroyer()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Destroy(child.gameObject);
        }
            flick = false;
        
    }
//______________________________________________________________________________________________________
}

        